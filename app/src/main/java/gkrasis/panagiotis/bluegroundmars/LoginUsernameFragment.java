package gkrasis.panagiotis.bluegroundmars;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import gkrasis.panagiotis.bluegroundmars.Model.UserSharedViewModel;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginUsernameFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginUsernameFragment extends Fragment {

    //Button to see password login form.
    @BindView(R.id.bt_login_next)
    Button btnNext;
    //Email EditText
    @BindView(R.id.et_username)
    EditText field_userName;

    private UserSharedViewModel userSVM;

    public LoginUsernameFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_username, container, false);
        ButterKnife.bind(this, view);

        userSVM = ViewModelProviders.of(getActivity()).get(UserSharedViewModel.class);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = field_userName.getText().toString().trim();
                if (validate(email)) {
                    userSVM.setEmail(email);

                    LoginPasswordFragment loginPasswordFragment = new LoginPasswordFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    if (fragmentManager != null) {
                        fragmentManager.beginTransaction()
                                .replace(R.id.login_fragment_container, loginPasswordFragment)
                                .addToBackStack(null)
                                .commit();
                    }
                } else {
                    //let the user know email is not valid
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage(R.string.dialog_message)
                            .setTitle(R.string.dialog_title);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        return view;
    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]{2,8}+@[0-9]{4}+\\.[A-Z]{2}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}
