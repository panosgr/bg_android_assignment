package gkrasis.panagiotis.bluegroundmars;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import gkrasis.panagiotis.bluegroundmars.Model.UnitData;
import gkrasis.panagiotis.bluegroundmars.Model.UnitSharedViewModel;

/**
 * An activity representing a single Unit detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link UnitListActivity}.
 */
public class UnitDetailActivity extends AppCompatActivity implements UnitDetailFragment.UnitBookedListener {

    private FragmentManager fragmentManager;
    private String unitBookedId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_detail);
        ButterKnife.bind(this);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            Drawable icon = getDrawable(R.drawable.ic_action_close);
            icon.setTint(getColor(android.R.color.white));
            actionBar.setHomeAsUpIndicator(icon);
        }



        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.

        if (savedInstanceState == null) {

            String access_token = getIntent().getStringExtra(UnitListActivity.INTENT_EXRA_ACCESS_TOKEN);
            String unitId = getIntent().getStringExtra(UnitDetailFragment.ARG_UNIT_ID);
//            boolean isBooked = getIntent().getBooleanExtra(UnitDetailFragment.ARG_UNIT_BOOKED, false);

//             Create the detail fragment and add it to the activity
            // using a fragment transaction.
            UnitDetailFragment fragment = new UnitDetailFragment();

            Bundle arguments = new Bundle();
            arguments.putString(UnitListActivity.INTENT_EXRA_ACCESS_TOKEN, access_token);
            arguments.putString(UnitDetailFragment.ARG_UNIT_ID, unitId);
//            arguments.putBoolean(UnitDetailFragment.ARG_UNIT_BOOKED, isBooked);
            fragment.setArguments(arguments);
            fragment.setBookedListener(this);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.unit_detail_fragment_container, fragment)
                    .commit();


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            Intent returnIntent = new Intent();
            if(unitBookedId!=null) {
                returnIntent.putExtra(UnitListActivity.INTENT_EXRA_BOOK_ID, unitBookedId);
                setResult(Activity.RESULT_OK, returnIntent);
            }else{
                setResult(Activity.RESULT_CANCELED, returnIntent);
            }
            finish();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        if(unitBookedId!=null) {
            returnIntent.putExtra(UnitListActivity.INTENT_EXRA_BOOK_ID, unitBookedId);
            setResult(Activity.RESULT_OK, returnIntent);
        }else{
            setResult(Activity.RESULT_CANCELED, returnIntent);
        }

        super.onBackPressed();

    }

    @Override
    public void unitBooked(String id) {

        unitBookedId = id;
        /*boolean isInList = false;
        for (UnitData unitData : mAdapter.getItems()) {
            if (unitData.getId().equals(id)) {
                isInList = true;
            }
        }
*/
    }
}
