package gkrasis.panagiotis.bluegroundmars.Model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import gkrasis.panagiotis.bluegroundmars.Network.NetworkService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserSharedViewModel extends ViewModel {

    private String email;
    private String password;
    private final MutableLiveData<User> access = new MutableLiveData<User>();
    private User mUser = new User();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        validate();
    }

    private void validate() {

        Map<String, String> credentials = new HashMap<String, String>();
        credentials.put("email", email);
        credentials.put("password", password);

        //make network call with given credentials
        NetworkService.getInstance()
                .getJSONApi()
                .loginUser(credentials)
                .enqueue(new Callback<User>() {

                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        if(response.isSuccessful()) {
                            User user = response.body();
                            mUser = user;

                            Log.d("response access_token ", user.token.accessToken );
                            Log.d("response name ", user.info.email );

                            setAccess(user);
                        }else{  //else, credentials were incorrect
                            setAccess(null);
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Log.d("response error ", t.toString());
                        t.printStackTrace();
                        setAccess(null); //if there was a problem with the call, don't give access
                    }
                });
    }

    private void setAccess(User user) {
        access.setValue(user);
    }

    public LiveData<User> hasAccess() {
        return access;
    }

    public String getUserAccessToken() {
        return mUser.token.getAccessToken();
    }

}
