package gkrasis.panagiotis.bluegroundmars.Model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gkrasis.panagiotis.bluegroundmars.Network.NetworkService;
import gkrasis.panagiotis.bluegroundmars.UnitDetailFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UnitSharedViewModel extends ViewModel {

    private static final String BASE_IMAGE_URL = "http://mars.theblueground.net";
    //global reference to current unit
    private Unit mUnit;
    //on the fly update of list for UnitListActivity
    MutableLiveData<List<UnitData>> unitList = new MutableLiveData<>();
    //on the fly update of unit fields for UnitDetailFragment
    MutableLiveData<UnitData> liveDataUnit = new MutableLiveData<>();

    /**
     * Creates Uri for Picasso Images globally
     *
     * @param imagePath API image ending.
     * @return uri to be called by Picasso
     */
    public Uri getImageUrl(String imagePath) {
        Uri builtUri = Uri.parse(BASE_IMAGE_URL).buildUpon()
                .appendEncodedPath(imagePath)
                .build();
        return builtUri;
    }

    /**
     * Method for pagination
     */
    public void changePageNum(String access_token, int page, int perPage) {
        getUnits(access_token, page, perPage);
    }

    /**
     * Observable Method for on the fly fetching of unit list
     *
     * @return dynamic list of units
     */
    public LiveData<List<UnitData>> getUnits(String access_token, int pagenum, int perPage) {
        pullData(access_token, pagenum, perPage);
        return unitList;
    }

    /**
     * Method that sets the field of a unit as booked.
     * Note: The list is modified here bc unitList is the list the Adapter consumes.
     * @param id the id of the unit being booked.
     */
    public void setUnitAsBooked(String id) {
        if(unitList!=null){
            List<UnitData> currentList = unitList.getValue();
            for (UnitData data : currentList) {
                if (data.getId().equals(id)){
                    data.setBooked(true);
                }
            }

            unitList.setValue(currentList);
        }
    }

    /**
     * Method for getting total number of units from API
     *
     * @return number used to calculate amount of pages
     */
    public int getNumOfResults() {
        if (mUnit == null) return 1;
        return mUnit.getMeta().getTotalCount();
    }

    /**
     * Update current unit reference for observer methods
     *
     * @param mUnit unit selcted by user
     */
    public void setCurrentUnit(UnitData mUnit) {
        liveDataUnit.setValue(mUnit);
    }

    /**
     * Observable Method for on the fly unit formatting
     *
     * @return unit data
     */
    public MutableLiveData<UnitData> getLiveDataUnit() {
        return liveDataUnit;
    }

    /**
     * Method call to update the already existing unit data
     */
    public void getBookingDetails(String access_token, String id) {
        addBookingDetails(access_token, id);
    }

    /**
     * Method for booking a unit
     */
    public void bookUnit(String mAccess_token, String id, Integer yearToBook) {
        makeBooking(mAccess_token, id, yearToBook);
    }

    private void pullData(String access_token, final int page, int perPage) {

        Log.d("pulldata", "Access token being used is " + access_token);
        //make network call through access token with specific page
        NetworkService.getInstance()
                .getJSONApi()
                .getUnits("Bearer " + access_token, page, perPage)
                .enqueue(new Callback<Unit>() {
                    @Override
                    public void onResponse(Call<Unit> call, Response<Unit> response) {
                        if (response.isSuccessful()) {
                            Unit unit = response.body();
                            mUnit = unit;
                            if (unit != null) {
                                if (page == 1) {
                                    unitList.postValue(unit.getData());
                                    return;
                                }
                                List<UnitData> currentData = unitList.getValue();
                                currentData.addAll(unit.getData());
                                unitList.postValue(currentData);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Unit> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void addBookingDetails(String access_token, String id) {

        //make network call through access token for a specific unit
        NetworkService.getInstance()
                .getJSONApi()
                .getUnitById("Bearer " + access_token, id)
                .enqueue(new Callback<UnitData>() {
                    @Override
                    public void onResponse(Call<UnitData> call, Response<UnitData> response) {
                        if (response.isSuccessful())
                            setCurrentUnit(response.body());
//                            liveDataUnit.postValue(response.body());

                    }

                    @Override
                    public void onFailure(Call<UnitData> call, Throwable t) {

                    }
                });
    }


    private void makeBooking(String mAccess_token, String id, Integer yearToBook) {

        Map<String, String> data = new HashMap<String, String>();
        data.put("unitId", id);
        data.put("year", String.valueOf(yearToBook));
        NetworkService.getInstance()
                .getJSONApi()
                .bookUnit("Bearer " + mAccess_token, data)
                .enqueue(new Callback<UnitData>() {
                    @Override
                    public void onResponse(Call<UnitData> call, Response<UnitData> response) {
                        if (response.isSuccessful()) {
                            UnitData unit = liveDataUnit.getValue();
                            unit.setYearBooked(response.body().getYearBooked());
                            unit.setReference(response.body().getReference());
                            unit.setBooked(true);
                            setCurrentUnit(unit);



                        }
                    }

                    @Override
                    public void onFailure(Call<UnitData> call, Throwable t) {

                    }

                });

    }


}
