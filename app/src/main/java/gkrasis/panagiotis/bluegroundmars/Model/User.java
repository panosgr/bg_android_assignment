package gkrasis.panagiotis.bluegroundmars.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("token")
    @Expose
    public Token token;
    @SerializedName("user")
    @Expose
    Info info;

}

class Token{


    @SerializedName("accessToken")
    @Expose
    String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


}

class Info{

    @SerializedName("email")
    @Expose
    String email;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("picture")
    @Expose
    String picture;
    @SerializedName("role")
    @Expose
    String role;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
