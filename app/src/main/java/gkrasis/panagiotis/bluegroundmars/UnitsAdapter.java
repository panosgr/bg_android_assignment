package gkrasis.panagiotis.bluegroundmars;

import android.content.res.Resources;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gkrasis.panagiotis.bluegroundmars.Model.UnitData;
import gkrasis.panagiotis.bluegroundmars.Model.UnitSharedViewModel;

public class UnitsAdapter extends ListAdapter<UnitData, UnitsAdapter.UnitsViewHolder> {

    private static final DiffUtil.ItemCallback<UnitData> DIFF_CALLBACK = new DiffUtil.ItemCallback<UnitData>() {
        @Override
        public boolean areItemsTheSame(@NonNull UnitData oldUnit, @NonNull UnitData newUnit) {
            return oldUnit.getId().equals(newUnit.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull UnitData oldUnit, @NonNull UnitData newUnit) {
            return oldUnit == newUnit;
        }

    };
    private final Resources res;

    RecyclerView mRecyclerView;


    //method to get a reference to the rv for scrolling
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerView = recyclerView;
    }

    private UnitSharedViewModel unitSharedViewModel;

    public UnitsAdapter(UnitSharedViewModel usvm, Resources resources, UnitSelectedListener listener) {
        super(DIFF_CALLBACK);
        unitSharedViewModel = usvm;
        res = resources;
        mUnitSelectedListener = listener;
    }

    @NonNull
    @Override
    public UnitsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_units_list, parent, false);

        UnitsViewHolder mUnitsVH = new UnitsViewHolder(view);
        return mUnitsVH;
    }

    @Override
    public void onBindViewHolder(@NonNull final UnitsViewHolder holder, int position) {

        final UnitData unit = getItem(position);
        if (unit != null) {
            holder.bind(unit);
            /*
                TODO (35): PROBABLY DELETE - see if search query is used through here
            if (unit.getEnglish().equals(searchQuery)) {
                mRecyclerView.smoothScrollToPosition(position + 3);
                holder.detailsLayout.setVisibility(View.VISIBLE);
                holder.detailsLayout.startAnimation(slide_down);

            }*/
        }

    }

    public interface UnitSelectedListener {
        public void getUnitSelectedID(String id);
    }

    UnitSelectedListener mUnitSelectedListener;

    public class UnitsViewHolder extends RecyclerView.ViewHolder {

        UnitData mUnit;

        @OnClick(R.id.cv_list_item)
        public void unitSelected() {
            mUnitSelectedListener.getUnitSelectedID(mUnit.getId());
            unitSharedViewModel.setCurrentUnit(mUnit);
        }

        /*@BindView(R.id.cv_list_item)
        CardView unitCardview;
        */
        @BindView(R.id.iv_image)
        ImageView backgroundImage;
        @BindView(R.id.tv_name)
        TextView unitName;
        @BindView(R.id.rb_rating)
        RatingBar unitRating;
        @BindView(R.id.tv_description)
        TextView unitDescription;
        @BindView(R.id.tv_region)
        TextView unitRegion;
        @BindView(R.id.tv_price)
        TextView unitPrice;
        @BindView(R.id.tv_book_success)
        TextView unitBooked;

        public UnitsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }


        public void bind(UnitData unit) {
            mUnit = unit;
            unitName.setText(unit.getName());
            unitDescription.setText(unit.getDescription());
            unitRating.setRating(unit.getRating());
            unitRegion.setText(unit.getRegion());
            unitPrice.setText(String.format(res.getString(R.string.price_template), String.valueOf(unit.getPrice())));
            Uri imageUri = unitSharedViewModel.getImageUrl(unit.getPictures().get(0));
            Picasso.with(backgroundImage.getContext()).load(imageUri)
                    .error(res.getDrawable(R.drawable.image_null_details_small, null))
                    .into(backgroundImage);

            checkIfBooked(unit);

        }

        private void checkIfBooked(UnitData unit) {
            if(unit.isBooked()){
                unitBooked.setVisibility(View.VISIBLE);
            }else{
                unitBooked.setVisibility(View.GONE);
            }
        }


    }
}
