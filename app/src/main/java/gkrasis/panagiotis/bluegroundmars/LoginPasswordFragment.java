package gkrasis.panagiotis.bluegroundmars;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import gkrasis.panagiotis.bluegroundmars.Model.User;
import gkrasis.panagiotis.bluegroundmars.Model.UserSharedViewModel;
import gkrasis.panagiotis.bluegroundmars.Network.NetworkStatusReceiver;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginPasswordFragment extends Fragment {


    //Password Label
    @BindView(R.id.tv_label_password)
    TextView label_pass;
    @BindView(R.id.bt_login_signin)
    Button btnSignIn;
    @BindView(R.id.et_password)
    EditText field_password;
    @BindView(R.id.pb_loadingbar)
    ProgressBar loadingBar;
    @BindView(R.id.ib_pass_lock)
    ImageButton lock;

    private static final int PASSWORD_LOCKED = 129;
    private static final int PASSWORD_SHOW = 144;

    private UserSharedViewModel userSVM;

    public LoginPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_password, container, false);
        ButterKnife.bind(this, view);
        userSVM = ViewModelProviders.of(getActivity()).get(UserSharedViewModel.class);
        userSVM.hasAccess().observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                Snackbar.make(getActivity().findViewById(R.id.login_cord_layout),
                        R.string.login_cred_check_msg, Snackbar.LENGTH_LONG)
                        .show();

                if(user != null){
                    userLogIn();
                }else {
                    //If user is null and app is online, then credentials wrong.
                    //Else then there's no connectivity
                    if(NetworkStatusReceiver.isAppOnline()) {
                        Snackbar.make(getActivity().findViewById(R.id.login_cord_layout),
                                R.string.login_cred_error_msg, Snackbar.LENGTH_LONG)
                                .show();
                    }else {
                        Snackbar.make(getActivity().findViewById(R.id.login_cord_layout),
                                R.string.login_internet_error_msg, Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
                loadingBar.setVisibility(View.GONE);
            }
        });

        label_pass.setText("Password for "+userSVM.getEmail());

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = field_password.getText().toString().trim();
                userSVM.setPassword(password);
                loadingBar.setVisibility(View.VISIBLE);
            }
        });

        //make password visible or locked when pressing the lock button
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(field_password.getInputType()==PASSWORD_LOCKED) {
                    field_password.setInputType(PASSWORD_SHOW);
                    lock.setImageTintList(getResources().getColorStateList(R.color.colorAccent,null));
                } else{
                    field_password.setInputType(PASSWORD_LOCKED);
                    lock.setImageTintList(getResources().getColorStateList(R.color.colorPrimary,null));
                }
            }
        });

        return view;
    }

    private void userLogIn() {
        Intent intent = new Intent(getContext(), UnitListActivity.class);
        intent.putExtra(UnitListActivity.INTENT_EXRA_ACCESS_TOKEN, userSVM.getUserAccessToken());
        startActivity(intent);

    }
}
