package gkrasis.panagiotis.bluegroundmars;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gkrasis.panagiotis.bluegroundmars.Model.UnitData;
import gkrasis.panagiotis.bluegroundmars.Model.UnitSharedViewModel;


/**
 * A fragment representing a single Unit detail screen.
 * This fragment is either contained in a {@link UnitListActivity}
 * in two-pane mode (on tablets) or a {@link UnitDetailActivity}
 * on handsets.
 */
public class UnitDetailFragment extends Fragment {

//    public static final String ARG_UNIT_BOOKED = ;
    @BindView(R.id.ll_booking_complete)
    LinearLayout bookingCompleteLayout;
    @BindView(R.id.tv_yob)
    TextView yearOfBooking;
    @BindView(R.id.tv_reference)
    TextView referenceNumber;
    @BindView(R.id.cl_unit_detail)
    ConstraintLayout bookReservationLayout;
    @BindView(R.id.rb_rating)
    RatingBar unitRating;
    @BindView(R.id.tv_description)
    TextView unitDescription;
    @BindView(R.id.tv_region)
    TextView unitRegion;
    @BindView(R.id.tv_price)
    TextView unitPrice;
    @BindView(R.id.tv_amenities)
    TextView unitAmenities;
    @BindView(R.id.spinner_years)
    Spinner unitAvailability;
    @BindView(R.id.pb_loadingbar)
    ProgressBar loadingBar;
    @BindView(R.id.btn_book)
    Button btnBook;
    @BindView(R.id.tv_cancellation_policy)
    TextView unitCancellationPolicy;

    public static final String ARG_UNIT_ID = "unit_id";
    private UnitSharedViewModel unitSharedViewModel;
    private String mUnitID;
    private String mAccess_Token;
    private Integer yearToBook;

    public interface UnitBookedListener {
        void unitBooked(String id);
    }

    private UnitBookedListener bookedListener;

    public void setBookedListener(UnitBookedListener listener){
        bookedListener = listener;
    }


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UnitDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments().containsKey(ARG_UNIT_ID) &&
                getArguments().containsKey(UnitListActivity.INTENT_EXRA_ACCESS_TOKEN)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mUnitID = getArguments().getString(ARG_UNIT_ID);
            mAccess_Token = getArguments().getString(UnitListActivity.INTENT_EXRA_ACCESS_TOKEN);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_unit_detail, container, false);
        ButterKnife.bind(this, rootView);


        Activity activity = this.getActivity();
        final CollapsingToolbarLayout clpToolbar = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        final ImageView backgroundImage = (ImageView) activity.findViewById(R.id.iv_clp_toolbar);


        unitSharedViewModel = ViewModelProviders.of(this).get(UnitSharedViewModel.class);
        unitSharedViewModel.getLiveDataUnit().observe(this, new Observer<UnitData>() {
            @Override
            public void onChanged(@Nullable UnitData unit) {

                if (unit != null) {
                    clpToolbar.setTitle(unit.getName());
                    Uri imageUri = unitSharedViewModel.getImageUrl(unit.getPictures().get(0));
                    Picasso.with(clpToolbar.getContext()).load(imageUri)
                            .error(getResources().getDrawable(R.drawable.image_null_details_small, null))
                            .into(backgroundImage);

                    if(unit.isBooked()){
                        bookingCompleteLayout.setVisibility(View.VISIBLE);
                        bookReservationLayout.setVisibility(View.GONE);

                        yearOfBooking.setText(
                                String.format(getString(R.string.yobooking)
                                        , String.valueOf(unit.getYearBooked())));
                        referenceNumber.setText(
                                String.format(getString(R.string.reference)
                                , unit.getReference()));

                        Snackbar.make(getActivity().findViewById(R.id.detail_coord_layout),
                                R.string.book_success, Snackbar.LENGTH_LONG)
                                .show();

                        bookedListener.unitBooked(unit.getId());

                    } else {
                        setUpUI(unit);
                    }
                }
            }
        });

        //call this method to change the booking, after the object in usvm is started being observed
        unitSharedViewModel.getBookingDetails(mAccess_Token, mUnitID);

        loadingBar.setVisibility(View.VISIBLE);

        return rootView;
    }

    private String format(String string) {
        String break_sign = "<br/>";
        String break_sign_replace = " ";
        return string.replace(break_sign, break_sign_replace);
    }

    private void setUpUI(final UnitData unit) {

        String cleanDesc = format(unit.getDescription());
        unitDescription.setText(cleanDesc);
        unitRating.setRating(unit.getRating());
        unitRegion.setText(unit.getRegion());
        unitPrice.setText(String.format(getString(R.string.price_template), String.valueOf(unit.getPrice())));
        String amenClean = listify(unit.getAmenities());
        unitAmenities.setText(String.format(getString(R.string.amenities), amenClean));
        unitCancellationPolicy.setText(String.format(getString(R.string.cancellation_policy), unit.getCancellation()));
        Integer[] years = unit.getAvailability().toArray(new Integer[unit.getAvailability().size()]);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(getContext(),
                android.R.layout.simple_spinner_item, unit.getAvailability());
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        unitAvailability.setAdapter(adapter);

        yearToBook = years[0]; //initialization in case no option is selected.
        unitAvailability.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearToBook = (Integer) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unitSharedViewModel.bookUnit(mAccess_Token, unit.getId(), yearToBook);
            }
        });

        loadingBar.setVisibility(View.GONE);

    }

    private String listify(List<String> amenities) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] words = amenities.toArray(new String[amenities.size()]);
        for (int i = 0; i < words.length; i++) {
            stringBuilder.append(" + " + words[i] + "\n\n");
        }
        return stringBuilder.toString();
    }


}
