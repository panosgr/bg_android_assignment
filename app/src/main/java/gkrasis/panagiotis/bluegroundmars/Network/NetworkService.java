package gkrasis.panagiotis.bluegroundmars.Network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import gkrasis.panagiotis.bluegroundmars.Model.User;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static NetworkService mInstance;
    private static final String BASE_URL = "http://mars.theblueground.net/api/";
    private Retrofit mRetrofit;

    private NetworkService() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public static NetworkService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }

    public BlueGroundJSONApi getJSONApi() {
        return mRetrofit.create(BlueGroundJSONApi.class);
    }
}