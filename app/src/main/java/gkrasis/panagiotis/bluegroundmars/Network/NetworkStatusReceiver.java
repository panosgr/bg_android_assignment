package gkrasis.panagiotis.bluegroundmars.Network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkStatusReceiver extends BroadcastReceiver {

    public static final String NETWORK_AVAIL = "Internet";
    private static boolean appOnline = false; //supposing user online so no empty state on first download

    public NetworkStatusReceiver() {
    }


    public static boolean isAppOnline() {
        return appOnline;
    }

    public static void setAppOnline(boolean appOnline) {
        NetworkStatusReceiver.appOnline = appOnline;
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        //Make sure the action is proper
        boolean isOK = intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION);

        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);


        NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (isConnected && isOK) {
            if (wifi.isAvailable() || mobile.isAvailable()) {
                setAppOnline(true);
                Log.d(NETWORK_AVAIL, "App is online");
            }
        } else {
            setAppOnline(false);
            Log.d(NETWORK_AVAIL, "App is offline");
        }

    }

}
