package gkrasis.panagiotis.bluegroundmars.Network;


import java.util.List;
import java.util.Map;

import gkrasis.panagiotis.bluegroundmars.Model.Unit;
import gkrasis.panagiotis.bluegroundmars.Model.UnitData;
import gkrasis.panagiotis.bluegroundmars.Model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BlueGroundJSONApi {

    @FormUrlEncoded
    @POST("auth/login")
    Call<User> loginUser(@FieldMap Map<String, String> credentials);

    @GET("units")
    Call<Unit> getUnits(@Header("Authorization") String access_token, @Query("page") int pageNum
            , @Query("perPage") int perPage);

    @GET("units/{id}")
    Call<UnitData> getUnitById(@Header("Authorization") String access_token, @Path("id") String id);

    @FormUrlEncoded
    @POST("units/book")
    Call<UnitData> bookUnit(@Header("Authorization") String access_token, @FieldMap Map<String, String> data);

}