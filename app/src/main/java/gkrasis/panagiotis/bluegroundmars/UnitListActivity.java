package gkrasis.panagiotis.bluegroundmars;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import gkrasis.panagiotis.bluegroundmars.Model.UnitData;
import gkrasis.panagiotis.bluegroundmars.Model.UnitSharedViewModel;
import gkrasis.panagiotis.bluegroundmars.Network.NetworkStatusReceiver;

import java.util.List;

/**
 * An activity representing a list of Units. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link UnitDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class UnitListActivity extends AppCompatActivity implements UnitsAdapter.UnitSelectedListener {

    private final static int INTENT_REQUEST_CODE = 126;
    public static String INTENT_EXRA_ACCESS_TOKEN = "access_token";
    public static String INTENT_EXRA_BOOK_ID = "booked_id";
    @BindView(R.id.rv_units)
    RecyclerView unitsRV;
    @BindView(R.id.list_fragment_container)
    FrameLayout listContainer;
    @BindView(R.id.tv_list_internet_error)
    TextView internetErrorMessage;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private UnitSharedViewModel usvm;
    private UnitsAdapter mAdapter;
    private boolean loading;
    private int totalPages;
    private int resultsPerPage = 10;
    private int currentPage = 1;
    private String access_token;
    private NetworkStatusReceiver mNSR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_list);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());


        //TODO(101): DELETE if logout is implemented.
        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (findViewById(R.id.unit_detail_fragment_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        checkInternetStatus();



        mNSR = new NetworkStatusReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        //(2) Broadcast Receiver Register
        registerReceiver(mNSR, intentFilter);

    }

    private void checkInternetStatus() {
        //if app is offline, show respective message
        if(!NetworkStatusReceiver.isAppOnline()){
            internetErrorMessage.setVisibility(View.VISIBLE);
            listContainer.setVisibility(View.GONE);
        }else {
            populateUI();
        }
    }

    public void populateUI(){
        internetErrorMessage.setVisibility(View.GONE);
        listContainer.setVisibility(View.VISIBLE);

        usvm = ViewModelProviders.of(this).get(UnitSharedViewModel.class);

        setupRecyclerView(unitsRV);

        access_token = getIntent().getStringExtra(INTENT_EXRA_ACCESS_TOKEN);

        //setup total amount of pages to use in pagination.
        usvm.changePageNum(access_token, currentPage, resultsPerPage);
        usvm.getUnits(access_token, currentPage, resultsPerPage).observe(this, new Observer<List<UnitData>>() {
            @Override
            public void onChanged(@Nullable List<UnitData> unitData) {
                mAdapter.submitList(null); //reset list bc ListAdapter needs new list
                mAdapter.submitList(unitData);
                if (currentPage != 1) { //continue in position where user left off
                    unitsRV.scrollToPosition(resultsPerPage * (currentPage - 1) - 1);
                }
                //set after result for dynamic instantiation and thread flexibility
                totalPages = usvm.getNumOfResults() / resultsPerPage;


            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //(3) Broadcast Receiver Unregister. Tried in onPause but crashes,BR not registered anymore
        unregisterReceiver(mNSR);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //TODO(100): REPLACE THIS WITH A LOGOUT OPTION, PREFERABLY FROM A NAVDRAWER
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        unitsRV.setLayoutManager(linearLayoutManager);
        mAdapter = new UnitsAdapter(usvm, getResources(), this);
        unitsRV.setAdapter(mAdapter);

        loading = true;

        unitsRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!unitsRV.canScrollVertically(1)) {
                    int itemsN = unitsRV.getAdapter().getItemCount();
                    loading = false;
                    if (currentPage < totalPages) {
                        currentPage++;
                        usvm.changePageNum(getIntent().getStringExtra(INTENT_EXRA_ACCESS_TOKEN), currentPage, resultsPerPage);
                    } else {
                        Toast.makeText(recyclerView.getContext(), "End of results", Toast.LENGTH_SHORT).show();
                    }
                }


                loading = true;
            }

        });

    }

    /**
     * Unit has been clicked, you can now start a new Fragment/Activity
     *
     * @param unitId the id of the unit
     */
    @Override
    public void getUnitSelectedID(String unitId) {

        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(INTENT_EXRA_ACCESS_TOKEN, access_token);
            arguments.putString(UnitDetailFragment.ARG_UNIT_ID, unitId);
//            arguments.putBoolean(UnitDetailFragment.ARG_UNIT_BOOKED, isBooked);
            UnitDetailFragment fragment = new UnitDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.unit_detail_fragment_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(this, UnitDetailActivity.class);
            intent.putExtra(INTENT_EXRA_ACCESS_TOKEN, access_token);
            intent.putExtra(UnitDetailFragment.ARG_UNIT_ID, unitId);
//            intent.putExtra(UnitDetailFragment.ARG_UNIT_BOOKED, isBooked);
            startActivityForResult(intent, INTENT_REQUEST_CODE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == INTENT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String id = null;
                if (data != null) {
                    id = data.getStringExtra(INTENT_EXRA_BOOK_ID);

                    //TODO: USE THIS METHOD TO REACH THE UNITLIST AND SET A NEW VALUE
                    usvm.setUnitAsBooked(id);
                    /*for(UnitData unit : usvm.getUnits().getValue()){
                        if(unit.getId().equals(id))
                            unit.setBooked(true);
                            Log.d("booked unit found! ", unit.getName());
                    }*/
                }
            } else {
                Log.d("booked id back is: ", "NOT BOOKED!");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkInternetStatus();
    }
}
