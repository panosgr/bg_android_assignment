package gkrasis.panagiotis.bluegroundmars;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import gkrasis.panagiotis.bluegroundmars.Model.UserSharedViewModel;
import gkrasis.panagiotis.bluegroundmars.Network.NetworkStatusReceiver;

public class LoginActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private NetworkStatusReceiver mNSR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        LoginUsernameFragment usernameFragment = new LoginUsernameFragment();
        fragmentTransaction.add(R.id.login_fragment_container, usernameFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

         /*Initiating BroadcastReceiver to receive connectivity change messages
         //Is up-to-date when the app is online or when it is offline */
        //(1)Broadcast receiver instance
        mNSR = new NetworkStatusReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        //(2) Broadcast Receiver Register
        registerReceiver(mNSR, intentFilter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //(3) Broadcast Receiver Unregister. Tried in onPause but crashes,BR not registered anymore
        unregisterReceiver(mNSR);
    }

}
